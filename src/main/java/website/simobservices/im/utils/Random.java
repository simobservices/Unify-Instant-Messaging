package website.simobservices.im.utils;

import java.security.SecureRandom;

public final class Random {

    public static final SecureRandom SECURE_RANDOM = new SecureRandom();

    private Random() {

    }

}
