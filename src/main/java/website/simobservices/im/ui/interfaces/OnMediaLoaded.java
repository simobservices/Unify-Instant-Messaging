package website.simobservices.im.ui.interfaces;

import java.util.List;

import website.simobservices.im.ui.util.Attachment;

public interface OnMediaLoaded {

    void onMediaLoaded(List<Attachment> attachments);
}
