package website.simobservices.im.ui;

public interface UiInformableCallback<T> extends UiCallback<T> {
    void inform(String text);
}
