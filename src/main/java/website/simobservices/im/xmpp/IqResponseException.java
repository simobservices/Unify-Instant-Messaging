package website.simobservices.im.xmpp;

public class IqResponseException extends Exception {

    public IqResponseException(final String message) {
        super(message);
    }
}
