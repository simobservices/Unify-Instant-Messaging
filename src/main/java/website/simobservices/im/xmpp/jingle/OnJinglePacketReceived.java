package website.simobservices.im.xmpp.jingle;

import website.simobservices.im.entities.Account;
import website.simobservices.im.xmpp.PacketReceived;
import website.simobservices.im.xmpp.jingle.stanzas.JinglePacket;

public interface OnJinglePacketReceived extends PacketReceived {
	void onJinglePacketReceived(Account account, JinglePacket packet);
}
