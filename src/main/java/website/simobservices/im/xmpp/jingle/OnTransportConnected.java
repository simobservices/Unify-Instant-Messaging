package website.simobservices.im.xmpp.jingle;

public interface OnTransportConnected {
	void failed();

	void established();
}
