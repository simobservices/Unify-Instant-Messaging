package website.simobservices.im.xmpp.jingle;

import website.simobservices.im.entities.Account;
import website.simobservices.im.xmpp.Jid;

public interface OngoingRtpSession {
    Account getAccount();
    Jid getWith();
    String getSessionId();
}
