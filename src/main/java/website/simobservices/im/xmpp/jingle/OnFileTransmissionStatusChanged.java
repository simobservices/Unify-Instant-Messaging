package website.simobservices.im.xmpp.jingle;

import website.simobservices.im.entities.DownloadableFile;

public interface OnFileTransmissionStatusChanged {
	void onFileTransmitted(DownloadableFile file);

	void onFileTransferAborted();
}
