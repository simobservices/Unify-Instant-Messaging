package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;
import website.simobservices.im.xmpp.stanzas.MessagePacket;

public interface OnMessagePacketReceived extends PacketReceived {
	void onMessagePacketReceived(Account account, MessagePacket packet);
}
