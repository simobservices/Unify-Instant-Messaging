package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;
import website.simobservices.im.xmpp.stanzas.PresencePacket;

public interface OnPresencePacketReceived extends PacketReceived {
	void onPresencePacketReceived(Account account, PresencePacket packet);
}
