package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Contact;

public interface OnContactStatusChanged {
	void onContactStatusChanged(final Contact contact, final boolean online);
}
