package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;

public interface OnMessageAcknowledged {
    boolean onMessageAcknowledged(Account account, Jid to, String id);
}
