package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;
import website.simobservices.im.xmpp.stanzas.IqPacket;

public interface OnIqPacketReceived extends PacketReceived {
	void onIqPacketReceived(Account account, IqPacket packet);
}
