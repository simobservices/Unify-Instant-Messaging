package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;

public interface OnBindListener {
	void onBind(Account account);
}
