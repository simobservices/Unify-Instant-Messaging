package website.simobservices.im.xmpp;

import website.simobservices.im.entities.Account;

public interface OnStatusChanged {
	void onStatusChanged(Account account);
}
