package website.simobservices.im.xmpp.stanzas.csi;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class ActivePacket extends AbstractStanza {
	public ActivePacket() {
		super("active");
		setAttribute("xmlns", Namespace.CSI);
	}
}
