package website.simobservices.im.xmpp.stanzas.csi;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class InactivePacket extends AbstractStanza {
	public InactivePacket() {
		super("inactive");
		setAttribute("xmlns", Namespace.CSI);
	}
}
