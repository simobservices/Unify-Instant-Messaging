package website.simobservices.im.xmpp.stanzas.streammgmt;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class EnablePacket extends AbstractStanza {

	public EnablePacket() {
		super("enable");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
		this.setAttribute("resume", "true");
	}

}
