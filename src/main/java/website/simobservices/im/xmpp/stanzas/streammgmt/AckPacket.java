package website.simobservices.im.xmpp.stanzas.streammgmt;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class AckPacket extends AbstractStanza {

	public AckPacket(final int sequence) {
		super("a");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
		this.setAttribute("h", Integer.toString(sequence));
	}

}
