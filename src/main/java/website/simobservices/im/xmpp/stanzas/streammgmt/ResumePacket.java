package website.simobservices.im.xmpp.stanzas.streammgmt;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class ResumePacket extends AbstractStanza {

	public ResumePacket(final String id, final int sequence) {
		super("resume");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
		this.setAttribute("previd", id);
		this.setAttribute("h", Integer.toString(sequence));
	}

}
