package website.simobservices.im.xmpp.stanzas.streammgmt;

import website.simobservices.im.xml.Namespace;
import website.simobservices.im.xmpp.stanzas.AbstractStanza;

public class RequestPacket extends AbstractStanza {

	public RequestPacket() {
		super("r");
		this.setAttribute("xmlns", Namespace.STREAM_MANAGEMENT);
	}

}
